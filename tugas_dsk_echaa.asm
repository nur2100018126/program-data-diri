.model small
.code
org 100h

jmp cetak
nama db  "Nama  : Nur Florisha Putri", 0dh, 0ah, '$'
nim db   "Nim   : 2100018126", 0dh, 0ah, '$'
kelas db "Kelas : C", 0dh, 0ah, '$' 
         
cetak:         
mov ah,09h
mov dx,offset nama
int 21h

mov ah,09h
mov dx,offset nim
int 21h

mov ah,09h
mov dx,offset kelas
int 21h        

end cetak
